<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use App\Tiket;
use Faker\Generator as Faker;

$factory->define(Tiket::class, function (Faker $faker) {
    return [
        'name_tiket' => $faker->word,
        'harga_tiket' => rand(1, 1000),
        'jenis_tiket' => rand(1, 30),
        'jml_tiket' => $faker->word,
        'id_category' => function () {
            return Category::all()->random();
        },
    ];
});
