<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Tiket;
use App\Transaksi;
use Faker\Generator as Faker;

$factory->define(Transaksi::class, function (Faker $faker) {
    return [
        'status' => rand(0, 1),
        'qty' => rand(1, 30),
        'id_tiket' => function () {
            return Tiket::all()->random();
        },
    ];
});
