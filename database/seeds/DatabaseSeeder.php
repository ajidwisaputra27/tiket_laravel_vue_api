<?php

use App\Category;
use App\Tiket;
use App\Transaksi;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 20)->create();
        factory(Category::class, 20)->create();
        factory(Tiket::class, 20)->create();
        factory(Transaksi::class, 20)->create();
    }
}
