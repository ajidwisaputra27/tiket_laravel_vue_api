<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tiket extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'id_category', 'id');
    }

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class, 'id_tiket', 'id');
    }
}
