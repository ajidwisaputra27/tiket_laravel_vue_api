<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function tiket()
    {
        return $this->hasMany(Tiket::class, 'id_category', 'id');
    }
}
