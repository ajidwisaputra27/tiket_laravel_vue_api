<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TransaksiResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_tiket' => $this->tiket->name_tiket,
            'qty' => $this->qty,
            'harga_tiket' => $this->tiket->harga_tiket,
            'status' => $this->status,
            'created_at' => Carbon::parse($this->created_at)->formatLocalized('%A, %d %B %Y'),
        ];
    }
}
