<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TiketResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_tiket' => $this->name_tiket,
            'harga_tiket' => $this->harga_tiket,
            'jenis_tiket' => $this->jenis_tiket,
            'jml_tiket' => $this->jml_tiket,
            'name_category' => $this->category->name_category,
            'created_at' => Carbon::parse($this->created_at)->formatLocalized('%A, %d %B %Y'),
        ];
    }
}
