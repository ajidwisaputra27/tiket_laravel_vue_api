<?php

namespace App\Http\Controllers;

use App\Http\Resources\TransaksiResource;
use App\Transaksi;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TransaksiController extends Controller
{
    public function index()
    {
        return TransaksiResource::collection(Transaksi::where('status', 0)->latest()->get());
    }

    public function store(Request $request)
    {
        $transaksi = Transaksi::create($request->all());
        return response(new TransaksiResource($transaksi), Response::HTTP_CREATED);
    }

    public function update()
    {
        $transaksi = Transaksi::where('status', 0);
        $transaksi->update([
            'status' => 1,
        ]);
        return response('Updated', Response::HTTP_CREATED);
    }

    public function destroy(Transaksi $transaksi)
    {
        $transaksi->delete();
        return response('Deleted', Response::HTTP_ACCEPTED);
    }
}
